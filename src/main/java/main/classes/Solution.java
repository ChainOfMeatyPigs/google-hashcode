package main.classes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

    public static int getScore(List<SignUp> signUps, DataLib dl) {
        return signUps
                .stream()
                .mapToInt(signUp -> signUp.getBookIds()
                        .stream()
                        .mapToInt(id -> dl.getBooks().get(id).getScore())
                        .sum())
                .sum();
    }

    public static void writeSolution(String path, List<SignUp> signUps) {
        File file = new File(path);
        try (PrintStream printStream = new PrintStream(new FileOutputStream(file))) {

            signUps = signUps
                    .stream()
                    .filter(signUp -> signUp.getBookIds().size() > 0)
                    .collect(Collectors.toList());

            // write amount of libraries used
            printStream.println(signUps.size());

            writeSignUps(printStream, signUps);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeSignUps(PrintStream printStream, List<SignUp> signUps) {
        for (SignUp signUp : signUps) {
            // libId and amount of books scanned
            printStream.print(signUp.getLibraryId());
            printStream.print(" ");
            printStream.println(signUp.getBookIds().size());

            printStream.println(signUp
                    .getBookIds()
                    .stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(" ")));
        }
    }
}
