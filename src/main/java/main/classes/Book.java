package main.classes;

public class Book implements Comparable<Book> {
	private final int id;
	private final int score;

	public Book(int id, int score) {
		this.id = id;
		this.score = score;
	}

	public int getId() {
		return id;
	}

	public int getScore() {
		return score;
	}

	@Override
	public String toString() {
		return "Book{" +
				"id=" + id +
				", score=" + score +
				'}';
	}

	@Override
	public int compareTo(Book o) {
		return Integer.compare(this.score, o.score);
	}
}
