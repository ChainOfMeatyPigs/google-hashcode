package main.classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Library {

    private final List<Book> books;

    private final int id;

    private final int signUpTime;

    private final int booksPerDay;

    public Library(List<Book> books, int id, int timeToScan, int booksPerDay) {
        this.id = id;

        Collections.sort(books);
        Collections.reverse(books);
        this.books = books;

        this.signUpTime = timeToScan;
        this.booksPerDay = booksPerDay;
    }

    public <T> Simulator<T> getSimulator(T init, Reducer<T> reducer) {
        return (start, amountOfDays) -> {
            T t = init;

            int amountOfBooksHandled = this.amountOfBooksHandled(start, amountOfDays);

            for (int index = 0; index < amountOfBooksHandled; index++) {
                Book book = books.get(index);
                t = reducer.next(t, book);
            }

            return t;
        };
    }

    public boolean canRunTheWholeTime(int start, int amountOfDays) {
        return amountOfBooksHandled(start, amountOfDays) == amountOfDays * booksPerDay;
    }

    public int amountOfBooksHandled(int start, int amountOfDays) {
        int daysLeft = amountOfDays - start;
        int daysToScan = daysLeft - signUpTime;
        return Math.min(daysToScan * booksPerDay, books.size());
    }

    public int getScoreFromSignUp(int start, int amountOfDays) {
        return this
                .getSimulator(0, (t, book) -> book.getScore())
                .simulate(start, amountOfDays);
    }

    public SignUp getSignUp(int start, int amountOfDays) {
        Reducer<List<Integer>> reducer = (t, book) -> {
            t.add(book.getId());
            return t;
        };

        List<Integer> books = this
                .getSimulator(new ArrayList<>(), reducer)
                .simulate(start, amountOfDays);

        return new SignUp(this.id, books);
    }

    public List<Book> getBooks() {
        return books;
    }

    public int getSignUpTime() {
        return signUpTime;
    }

    public int getBooksPerDay() {
        return booksPerDay;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Library: ").
                append(id).
                append("\nSignup Time: ").append(signUpTime)
                .append("\nBooks Per Day: ").append(booksPerDay)
                .append("\n");
        for (Book b : books) {
            s.append("\t").append(b).append("\n");
        }
        return s.toString();
    }

    public interface Simulator<T> {
        T simulate(int start, int amountOfDays);
    }

    @FunctionalInterface
    public interface Reducer<T> {
        T next(T t, Book book);
    }
}
