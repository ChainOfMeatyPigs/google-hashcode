package main.classes;

import java.util.Collections;
import java.util.List;

public class DataLib {
	private final List<Library> libs;
	private final int amountOfDays;
	private final List<Book> books;

	public DataLib(List<Library> libs, int deadline, List<Book> books) {
		Collections.sort(libs);
		this.libs = libs;
		this.amountOfDays = deadline;
		this.books = books;
	}

	public List<Library> getLibs() {
		return libs;
	}

	public int getAmountOfDays() {
		return amountOfDays;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder("Datalib: ").append("Deadline: ").append(amountOfDays).append("\n");
		for(Library l : libs){
			s.append(l).append(", ").append("\n");
		}
		return s.toString();
	}

	public List<Book> getBooks() {
		return books;
	}
}
