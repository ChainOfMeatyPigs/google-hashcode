package main.classes;

import java.util.List;

public class SignUp {
    private int libraryId;
    private List<Integer> bookIds;

    public SignUp(int libraryId, List<Integer> bookIds) {
        this.libraryId = libraryId;
        this.bookIds = bookIds;
    }

    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    public List<Integer> getBookIds() {
        return bookIds;
    }

    public void setBookIds(List<Integer> bookIds) {
        this.bookIds = bookIds;
    }
}
