package main.io;

import main.classes.Book;
import main.classes.DataLib;
import main.classes.Library;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Parser {

	public static DataLib parseFile(String path) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
		String[] line = reader.readLine().split(" ");
		int aantalBoeken = Integer.parseInt(line[0]);
		int aantalLibs = Integer.parseInt(line[1]);
		int deadline = Integer.parseInt(line[2]);
		List<Book> books = createBooks(aantalBoeken, reader);
		List<Library> libs = createLibraries(reader, aantalLibs, books);
		DataLib dl = new DataLib(libs, deadline, books);
		reader.close();
		return dl;
	}

	private static List<Library> createLibraries(BufferedReader reader, int aantalLibs, List<Book> allBooks) throws IOException {
		List<Library> libs = new ArrayList<>();
		String[] line;
		for (int i = 0; i < aantalLibs; i++) {
			line = reader.readLine().split(" ");
			int aantalBoekenInLib = Integer.parseInt(line[0]);
			int signupTime = Integer.parseInt(line[1]);
			int shipBooksPerDay = Integer.parseInt(line[2]);


			line = reader.readLine().split(" ");
			List<Book> booksInLib = new ArrayList<>();
			for (int j = 0; j < aantalBoekenInLib; j++) {
				booksInLib.add(allBooks.get(Integer.parseInt(line[j])));
			}

			Library lib = new Library(booksInLib, i, signupTime, shipBooksPerDay);
			libs.add(lib);
		}
		return libs;
	}

	private static List<Book> createBooks(int aantalBoeken, BufferedReader reader) throws IOException {
		String[] line = reader.readLine().split(" ");
		if(line.length != aantalBoeken) throw new Error("Geen correct aantal boeken");
		List<Book> books = new ArrayList<>();
		for (int i = 0; i < aantalBoeken; i++) {
			books.add(new Book(i, Integer.parseInt(line[i])));
		}
		return books;
	}
}
