package main;

import main.classes.DataLib;
import main.classes.Library;
import main.classes.SignUp;
import main.classes.Solution;
import main.io.Parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Main {

    private static final String FILENAME = "d_tough_choices.txt";
    private static final String[] FILENAMES = {
            "a_example.txt",
            "b_read_on.txt",
            "c_incunabula.txt",
            "d_tough_choices.txt",
            "e_so_many_books.txt",
            "f_libraries_of_the_world.txt"
    };

    private static final Random RG = new Random();

    public static void main(String[] args) throws IOException {
        System.out.println("yeet");
//    	runForFile(FILENAME);
        for (String fileName : FILENAMES) {
            System.out.println("file: " + fileName);
            randomKak(fileName);
        }
    }

    private static void randomKak(String fileName) throws IOException {
        DataLib dl = Parser.parseFile("in/" + fileName);

        int amountOfDays = dl.getAmountOfDays();
        AtomicInteger currentDay = new AtomicInteger(0);

        List<Library> libs = dl.getLibs();

        int score = 0;

        List<SignUp> signUps = new ArrayList<>();
        while(currentDay.get() < amountOfDays && dl.getLibs().size() > 0){
            int index = RG.nextInt(libs.size());
            signUps.add(libs.get(index).getSignUp(currentDay.get(), amountOfDays));
            libs.remove(index);

        }

        Solution.writeSolution("solutions/" + fileName, signUps);
    }

//    public static void runForFile(String fileName) throws IOException {
//        DataLib dl = Parser.parseFile("in/" + fileName);
////		System.out.println(dl);
//
//        int amountOfDays = dl.getAmountOfDays();
//        AtomicInteger currentDay = new AtomicInteger(0);
//
//        List<SignUp> signUps = solve(currentDay, amountOfDays, dl);
//
//        Solution.writeSolution("solutions/" + fileName, signUps);
//
//        System.out.println(currentDay);
//    }

    private static List<SignUp> solve(AtomicInteger currentDay, int amountOfDays, DataLib dl) {
        List<SignUp> signUps = new ArrayList<>();

        while (currentDay.get() < amountOfDays && dl.getLibs().size() > 0) {
            List<Library> possibleLibs = dl
                    .getLibs()
                    .stream()
                    .filter(l -> l.canRunTheWholeTime(currentDay.get(), amountOfDays))
                    .collect(Collectors.toList());

            Library max = possibleLibs.size() > 0 ? possibleLibs
                    .get(possibleLibs.size() - 1) : dl
                    .getLibs()
                    .stream()
                    .max((lib1, lib2) -> {
                        int score1 = lib1.getScoreFromSignUp(currentDay.get(), amountOfDays);
                        int score2 = lib2.getScoreFromSignUp(currentDay.get(), amountOfDays);

                        return Integer.compare(score1, score2);
                    })
                    .orElse(null);

            signUps.add(max.getSignUp(currentDay.get(), amountOfDays));

            dl.getLibs().remove(max);

            currentDay.addAndGet(max.getSignUpTime());
        }
        return signUps;
    }

    public static void runForFile(String fileName) throws IOException {
        DataLib dl = Parser.parseFile("in/" + fileName);
//		System.out.println(dl);

        int amountOfDays = dl.getAmountOfDays();
        AtomicInteger currentDay = new AtomicInteger(0);

        int score = 0;

        List<SignUp> signUps = new ArrayList<>();

        while (currentDay.get() < amountOfDays && dl.getLibs().size() > 0) {
            Library max = dl
                    .getLibs()
                    .stream()
                    .max(Comparator.comparing(l1-> l1.getAantalBoeken(currentDay.get(), amountOfDays)))
                    .orElse(null);

            signUps.add(max.getSignUp(currentDay.get(), amountOfDays));

            dl.getLibs().remove(max);

            score = max.getScoreFromSignUp(currentDay.get(), amountOfDays);

            currentDay.addAndGet(max.getSignUpTime());
        }

        Solution.writeSolution("solutions/" + fileName, signUps);

        System.out.println(currentDay);
        System.out.println(score);
    }
}
